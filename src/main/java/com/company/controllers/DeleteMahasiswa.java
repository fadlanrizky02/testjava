package com.company.controllers;

import com.company.model.MahasiswaDB;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DeleteMahasiswa extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        MahasiswaDB mb = new MahasiswaDB();

        Map where = new HashMap();
        where.put("id",Integer.parseInt(request.getParameter("id")));

        mb.delete("mahasiswa",where);
        response.sendRedirect("/Tugas1/TampilMahasiswa");
    }
}
