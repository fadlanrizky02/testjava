package com.company.controllers;

import com.company.model.MahasiswaDB;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;

public class TampilMahasiswa extends HttpServlet {
    MahasiswaDB mb = new MahasiswaDB();
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            ResultSet results = mb.get("mahasiswa");
            req.setAttribute("val",results);
            RequestDispatcher request = req.getRequestDispatcher("tampil.jsp");
            request.forward(req,res);
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
