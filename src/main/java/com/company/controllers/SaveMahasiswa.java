package com.company.controllers;

import com.company.model.MahasiswaDB;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveMahasiswa extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rs = request.getRequestDispatcher("form.html");
        rs.forward(request,response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Map data = new HashMap();
        data.put("fullname",request.getParameter("fullname"));
        data.put("address",request.getParameter("address"));
        data.put("status",request.getParameter("status"));
        data.put("gradesphysics",Integer.parseInt(request.getParameter("gradesphysics")));
        data.put("gradescalculus",Integer.parseInt(request.getParameter("gradescalculus")));
        data.put("gradesbiologi",Integer.parseInt(request.getParameter("gradesbiologi")));

        MahasiswaDB mb = new MahasiswaDB();
        mb.insert("mahasiswa",data);
        response.sendRedirect("/Tugas1/TampilMahasiswa");
    }
}
